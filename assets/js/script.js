/*
Template Name: Consulex
Author: TrendyTheme
Version: 1.0
*/

;(function () {

	"use strict"; // use strict to start

	$(document).ready(function () {


	    /* ======= Preloader ======= */
	    (function () {
	        $('#status').fadeOut();
	        $('#preloader').delay(200).fadeOut('slow');
	    }());



		if($('.scroll-top').length > 0){
			$('a.scroll-top').on('click', function(event) {
    			var $anchor = $(this);
    			$('html, body').stop().animate({
        			scrollTop: $($anchor.attr('href')).offset().top - 60
    			}, 1500, 'easeInOutExpo');
    				event.preventDefault();
			});
		}


		//owl carousel
		if($('.testimonial-carousel'). length > 0){
			$(".testimonial-carousel").owlCarousel({
				navigation : true,
				pagination: false,
				items : 2,
				itemsDesktop: [1199,2],
				itemsDesktopSmall: [979,1],
				itemsTablet: [768,1],
				navigationText: ["", ""],
				autoHeight: true 
			});
		}
	  

		//Menu script
		if($('.menuzord').length > 0){
			$(".menuzord").menuzord({
				align: "left",
				effect: "slide"
			});
		}


		//Sticky Menu
		if($('.main-navigation').length > 0){
			$('.main-navigation').sticky({
				topSpacing: 0
			});
		}

		/* === Tab to Collapse === */
        if ($('.nav-tabs').length > 0) {
           $('.nav-tabs').tabCollapse();
        }



        /* === Detect IE version === */
        (function () {
            
            function getIEVersion() {
                var match = navigator.userAgent.match(/(?:MSIE |Trident\/.*; rv:)(\d+)/);
                return match ? parseInt(match[1], 10) : false;
            }

            if( getIEVersion() ){
                $('html').addClass('ie'+getIEVersion());
            }

        }());



		/* === Gallery Thumb Carousel === */
		if ($('.img-slider').length > 0) {
			$('.gallery-thumb').flexslider({
				animation: "slide",
				controlNav: "thumbnails",
			});
		}


		/* === Progress Bar === */
		$('.progress').on('inview', function(event, visible, visiblePartX, visiblePartY) {
			if (visible) {
				$.each($('div.progress-bar'),function(){
					$(this).css('width', $(this).attr('aria-valuenow')+'%');
				});
				$(this).off('inview');
			}
		});


		/*==========fun facts =========== */
		$('#fact-one').on('inview', function(event, visible, visiblePartX, visiblePartY) {
			if (visible) {
				$(this).find('.timer').each(function () {
					var $this = $(this);
					$({ Counter: 0 }).animate({ Counter: $this.text() }, {
						duration: 2000,
						easing: 'swing',
						step: function () {
							$this.text(Math.ceil(this.Counter));
						}
					});
				});
				$(this).off('inview');
			}
		});


		$('.fun-fact-section').on('inview', function(event, visible, visiblePartX, visiblePartY) {
			  if (visible) {
				  $(this).find('.timer').each(function () {
					  var $this = $(this);
					  $({ Counter: 0 }).animate({ Counter: $this.text() }, {
						  duration: 2000,
						  easing: 'swing',
						  step: function () {
							  $this.text(Math.ceil(this.Counter));
						  }
					  });
				  });
				  $(this).off('inview');
			  }
		});



        /* === magnificPopup === */
        if ($('.tt-lightbox').length > 0) {
            $('.tt-lightbox').magnificPopup({
                type: 'image',
                mainClass: 'mfp-fade',
                removalDelay: 160,
                fixedContentPos: false
                // other options
            });
        }


        if ($('.popup-video').length > 0) {
            $('.popup-video').magnificPopup({
                disableOn: 700,
                type: 'iframe',
                mainClass: 'mfp-fade',
                removalDelay: 160,
                preloader: false,
                fixedContentPos: false
            });
        }




	    $('.photoStream').jflickrfeed({
	        limit: 6,
	        qstrings: {
	            id: '52617155@N08'
	        },
	        itemTemplate: '<li>'+
	                        '<a href="{{image}}" title="{{title}}">' +
	                            '<img src="{{image_s}}" alt="{{title}}" />' +
	                        '</a>' +
	                      '</li>'
	    });


		/* ======= Stellar for background scrolling ======= */
		if ($('.parallax-bg').length > 0) {
	    	$('.parallax-bg').imagesLoaded( function() {

	    		$(window).stellar({
	                		horizontalScrolling: false,
	                		verticalOffset: 0,
	                		horizontalOffset: 0,
	                		responsive: true,
	                		hideDistantElements: true
	        		});
	    	});
		}


        /* ======= Contact Form ======= */
        $('#contactForm').on('submit',function(e){

            e.preventDefault();

            var $action = $(this).prop('action');
            var $data = $(this).serialize();
            var $this = $(this);

            $this.prevAll('.alert').remove();

            $.post( $action, $data, function( data ) {

                if( data.response=='error' ){

                    $this.before( '<div class="alert danger-border"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <i class="fa fa-times-circle"></i> '+data.message+'</div>' );
                }

                if( data.response=='success' ){

                    $this.before( '<div class="alert success-border"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="fa fa-thumbs-o-up"></i> '+data.message+'</div>' );
                    $this.find('input, textarea').val('');
                }

            }, "json");

        });



	 
		$(window).on('load', function () {

		    /* ======= shuffle js ======= */
		    if ($('.filterable-portfolio').length > 0) {
		        /* initialize shuffle plugin */
		        var $grid = $('.filterable-portfolio');

		        $grid.shuffle({
		            itemSelector: '.portfolio-item' // the selector for the items in the grid
		        });

		        /* reshuffle when user clicks a filter item */
		        $('.portfolio-filter li').on('click', function (e) {
		            e.preventDefault();

		            // set active class
		            $('.portfolio-filter li').removeClass('active');
		            $(this).addClass('active');

		            // get group name from clicked item
		            var groupName = $(this).attr('data-group');

		            // reshuffle grid
		            $grid.shuffle('shuffle', groupName );
		        });
		    }

		});


	});
})(jQuery);
